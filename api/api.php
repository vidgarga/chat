<?php 

define("ROOT", dirname(__FILE__));
$path_controllers = ROOT."/controllers";

foreach (scandir($path_controllers) as $filename) {
    $path = $path_controllers . '/' . $filename;
    
    if (is_file($path)) {
        require_once $path;
    }
}

$params = explode("/", $_GET["url"]);
$controller = $params[0]."Controller";
new $controller;