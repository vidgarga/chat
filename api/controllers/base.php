<?php

class BaseController
{
	protected $method, $controller;
	protected $model;

	public function __construct()
	{
		$this->method = $_SERVER['REQUEST_METHOD'];

		$this->requireModel();

		$this->process();
	}

	public function requireModel()
	{
		$path_model = ROOT."/models/".$this->model.".php";
		if (is_file($path_model)) {
        	require_once $path_model;
    	}
	}

	public function process()
	{
		$result;
		$params = explode("/", $_GET["url"]);
		switch($this->method)
		{
			case "GET":
				$result = $this->get($params[1]);
			break;
			case "POST":
				$this->post($_POST["time"], $_POST["user"], $_POST["message"]);
			break;
		}

		echo json_encode($result);
	}
	
	public function displayContent()
	{
		parent::displayContent();
		self::$smarty->display(_PS_THEME_DIR_.'index.tpl');
	}
}