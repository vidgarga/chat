<?php

class ChatController extends BaseController
{
	protected $file;

	public function __construct()
	{
		$this->file = ROOT."/bbdd/chat.txt";
		$this->model = 'chat';

		parent::__construct();
	}
	
	public function get($time)
	{
		$handle = fopen($this->file, "r");
		$result = [];
		if ($handle) {
	    	while (($line = fgets($handle)) !== false) {
	    	
	    		$chat = new ChatModel($line);

	    		if($chat->time > $time) {
    				array_push($result, $chat->get());
	    		}
	    	}

    		fclose($handle);
		}

		return $result;
	}
	
	public function post($time, $user, $message)
	{
		$this->add($time, $user, $message);
	}

	public function add($time, $user, $message)
	{
		$handle = fopen($this->file, "a");
		if ($handle) {
			$result = ["time" => $time, "user" => $user, "message"=> $message];

			fwrite($handle, json_encode($result) . PHP_EOL); 
		}
	}
}