<?php 

class ChatModel
{
	public $time, $user, $message;


	public function __construct($line)
	{
		$this->set($line);
	}	

	public function get()
	{
		$result = ["time" => $this->time, "user" => $this->user, "message"=> $this->message];
		return $result;
	}

	public function set($line)
	{
		$result = json_decode($line);
		
		$this->time = $result->time;
		$this->user = $result->user;
		$this->message = $result->message;
	}
}