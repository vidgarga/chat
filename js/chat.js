function Chat(userName) {

	this.userName = userName;
    
    this._init();
}

Chat.prototype._init = function () {

	this.time = this._getTimeNow();

	$('.msg_container_base, .top-bar, .panel-footer').html('');

	this._drawForm();

	this._addEvents();

	this._titleChat();

	this._drawButtons();

	this._getData();
}

Chat.prototype._addEvents = function () {

	var self = this;

	$('#message').attr('autocomplete', 'off');
};

Chat.prototype._titleChat = function () {

	var titleHtml = '<i class="glyphicon glyphicon-comment"></i> Chat - ' + this.userName;
	$('<div />', {'class': 'col-md-8 col-xs-8', 'html': '<h3 class="panel-title">' + titleHtml + '</h3>'}).appendTo('.top-bar');
};

Chat.prototype._drawButtons = function () {

	var minDiv = $('<div />', {'class': 'col-md-4 col-xs-4 text-right'}).appendTo('.top-bar');

	$('<a />', {'href': '#', 'html': '<span class="glyphicon icon_minim glyphicon-minus"></span>'}).on('click', function(event) {
		event.preventDefault();
		$('.panel-body, .panel-footer').toggleClass('hide');
	}).appendTo($(minDiv));
};

Chat.prototype._drawForm = function () {

	var self = this;

	$form = $('<form />', {'id': 'sendForm'}).
	on('submit', function(event) {
		event.preventDefault();
		self._submitChat();
	}).appendTo('.panel-footer');

	$group = $('<div />', {'class': 'input-group', 'html': '<input id="message" type="text" class="form-control input-sm chat_input" placeholder="Write your message here...">'}).appendTo($form);

	$span = $('<span />', {'class': 'input-group-btn'}).appendTo($group);
	
	$('<button />', {'class': 'btn btn-primary btn-sm', 'id':'btn-chat', 'html': 'Send'}).on('click', function(event) {
		event.preventDefault();
		self._submitChat();
	}).appendTo($span);
};

Chat.prototype._getData = function () {

	var self = this;

	$.ajax({
	   type: "GET",
	   url: "api/chat/" + self.time,
	   dataType: "json",
	   success: function(data){
	   	   self._updateChat(data);
	   	   setTimeout(self._getData(),3000);
	   },
	});
}

Chat.prototype._updateChat = function (data) {

	var self = this;

	$.each(data, function(i, line) {

		self.time = line.time;

		$('.msg_container_base').append('<div class="row msg_container base_sent">' +
                        		'<div class="col-md-10 col-xs-10 ">' +
                            	'<div class="messages msg_sent">' +
								'<b>' + line.user + '</b>' +
                                '<p>' + line.message + '</p>' +
                                '<time data-time="">' + moment(line.time, "YYYYMMDD HH:mm:ss").fromNow() + '</time>' +
                            	'</div>' +
                        		'</div>' +
                        		'<div class="col-md-2 col-xs-2 avatar">' +
                            	'<img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">' +
                        		'</div>' +
                    			'</div>');
	});
}

Chat.prototype._submitChat = function () {

		var self = this;
		

		var message_txt = $('#message').val();
                
        if(message_txt) {
              
         	$('#message').val(''); 

        	$.post( "api/chat", { time: self._getTimeNow(), user:  self.userName, message: message_txt })
            	.done(function( data ) {
                            
            });

        }
}

Chat.prototype._getTimeNow = function () {

	return moment().format('YYYYMMDDHHmmss');
}