# Simple Chat

Easy chat structure, using txt file to simulate bbdd

## Getting Started

--

### Prerequisites

Apache server with mod_rewrite module enabled
Its recommended to set the api/bbdd/chat.txt file to 777


### Installing

---

## Built With

* PHP
* Jquery
* Bootstrap

## Author

* **vidgarga** - bitbucket.org/vidgarga

## License

Free
